import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class Auth {
  public token: any;

  constructor(public http: Http, public storage: Storage) { }

  /**
   * Check User Authentication with token
   * API call to Get User Details
   * Save User Details in localstorage
   */
  checkAuthentication() {
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
        this.token = value;
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);
        this.http.get('https://indotechbeta4.herokuapp.com/api/users/me', { headers: headers })
          .subscribe(res => {
            let data = res.json();
            this.storage.set('user_details', data);// set storage for logged in user
            resolve(data);
          }, (err) => {
            reject(err);
          });
      });
    });
  }

  /**
   * Login API Call
   * Generate Token
   * @param credentials Username , Passworrd
   */
  login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      //headers.append('X-XSRF-Token', 'kK4GDdiHAHejPeoXl4dL1AAgb13eiEwPhxvDw=');
      let options = new RequestOptions({ headers: headers });
      this.http.post('https://indotechbeta4.herokuapp.com/auth/local', credentials, options)
        .subscribe(res => {
          let data = res.json();
          this.storage.set('token', data.token);// set local storage for token
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  /**
   * Logout
   */
  logout() {
    this.storage.set('token', '');
    this.storage.set('user_details', '');
  }

  /**
   * API call to get meterdatas based on user id
   * @param userid 
   */
  getMeterDatas(userid) {
    return new Promise((resolve, reject) => {
      this.http.get('https://indotechbeta4.herokuapp.com/api/meterdatas/' + userid)
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  /**
   * API call to get meter Details based on meter id
   * @param meterid 
   */
  meterdetails(meterid) {
    return new Promise((resolve, reject) => {
      this.http.get('https://indotechbeta4.herokuapp.com/api/meter_serial_details/' + meterid)
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  /**
   * API call to get meter load details based on meter id
   * @param meterid 
   */
  meterloaddetails(meterid) {
    return new Promise((resolve, reject) => {
      this.http.get('https://indotechbeta4.herokuapp.com/api/meter_serial_loads/s/' + meterid + '/1')
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  /**
   * API call to get meter serial load details based on meter id
   * @param meterid 
   * @param daysback 
   */
  meterserialloaddetails(meterid, daysback) {
    return new Promise((resolve, reject) => {
      this.http.get('https://indotechbeta4.herokuapp.com/api/meter_serial_loads/ls/' + meterid + '/' + daysback)
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }
}
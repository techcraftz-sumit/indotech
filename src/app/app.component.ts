import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login-page/login-page';
import { MeterPage } from '../pages/meter-page/meter-page';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  loggedIn: any;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      setTimeout(() => {
        splashScreen.hide();
      }, 100);

      this.storage.get('token').then((isLoggedIn) => {
        console.log('Is Logged in : ', isLoggedIn);
        this.loggedIn = isLoggedIn;
        if (!this.loggedIn) {
          this.rootPage = LoginPage;
        }
        else if (this.loggedIn) {
          this.rootPage = HomePage;
        }
      });
    });
  }
}


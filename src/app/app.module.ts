import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login-page/login-page';
import { MeterPage } from '../pages/meter-page/meter-page';
import { MeterOverview } from '../pages/meter-overview/meter-overview';
import { MeterDayView } from '../pages/meter-day-view/meter-day-view';
import { MeterWeekView } from '../pages/meter-week-view/meter-week-view';
import { HttpModule } from '@angular/http';
import { Auth } from '../providers/auth';
import {ChartModule} from 'angular2-highcharts';
import * as highcharts from 'Highcharts';
import { PopoverContentPage } from '../pages/popover/popover';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MeterPage,
    MeterOverview,
    MeterDayView,
    MeterWeekView,
	PopoverContentPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ChartModule.forRoot(highcharts)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MeterPage,
    MeterOverview,
    MeterDayView,
    MeterWeekView,
	PopoverContentPage
  ],
  providers: [
    Storage,
    StatusBar,
    SplashScreen,
    Auth,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }

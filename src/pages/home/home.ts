import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login-page/login-page';
import { MeterPage } from '../meter-page/meter-page';
import { PopoverController } from 'ionic-angular';
import { PopoverContentPage } from '../popover/popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public username: any;
  public userid: any;
  public meters: any;
  public d: any;
  public date: any;
  loading: any;
  loggedIn: any;
  constructor(public navCtrl: NavController, public authService: Auth, public storage: Storage, public loadingCtrl: LoadingController, public popoverCtrl: PopoverController) {
  }

  /**
   * Set user variables from localstoage
   */
  ngOnInit() {
    this.storage.get('user_details').then((val) => {
      this.username = val.name;
      this.userid = val._id;
      this.Loadmeters(this.userid);
    });
  }

  /**
   * Load meters based on user id
   * @param userid 
   */
  Loadmeters(userid) {
    this.showLoader();
    this.authService.getMeterDatas(userid).then((res) => { //function call from auth.ts for API
      this.meters = res;
      this.loading.dismiss();
    }, (err) => {
      console.log(err);
      this.loading.dismiss();
    });
  }


  timeAgo(hexString) {
    this.d = new Date();
    this.date = new Date(2000 + hexString.charCodeAt(5), hexString.charCodeAt(4) - 1, hexString.charCodeAt(3), hexString.charCodeAt(2), hexString.charCodeAt(1), hexString.charCodeAt(0));

    var seconds = Math.floor((this.d - this.date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
      return interval + " years";
    }

    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + " months";
    }

    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + " days";
    }

    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + " hours";
    }

    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }

  /**
   * Function call on anchor click 
   * Pass meter id to meter detail page
   * @param meterid 
   */
  DetailMeterData(meterid) {
    this.navCtrl.push(MeterPage, {
      meterid: meterid
    });
  }

  /**
   * Pop Over for Nav Controller
   * @param myEvent 
   */
  openPopover(myEvent) {
    this.storage.get('user_details').then((val) => {
      let popover = this.popoverCtrl.create(PopoverContentPage);
      popover.present({
        ev: myEvent
      });
    });
  }

  /**
   * Show loading content on page load
   */
  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';


@IonicPage()
@Component({
  selector: 'page-meter-week-view',
  templateUrl: 'meter-week-view.html',
})
export class MeterWeekView {
  meterid: any;
  meterdetails: any;
  noofdaysforChart = 0;
  chartOptions: any;
  loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: Auth, public loadingCtrl: LoadingController) {
    this.meterid = this.navParams.data;
  }

  /**
   * Get meter serial Load Details based on meter id
   */
  ngOnInit() {
    this.showLoader();
    this.authService.meterdetails(this.meterid).then((res) => {
      this.meterdetails = res[0];
      this.weeklyConsumptionChart(0);
      this.loading.dismiss();
    }, (err) => {
      console.log(err);
      this.loading.dismiss();
    });

  }

  /**
   * Weekly meter consumption chart
   * @param noofdaysforChart 
   */
  weeklyConsumptionChart(noofdaysforChart) {
    //sample json for highchart purpose to be built later
    const chartkwh = [];
    for (var i = 0; i < 7; i++) {
      chartkwh.push(this.meterdetails.endOfDayKwh[noofdaysforChart].kwh[i]);
    }
    this.chartOptions = {
      chart: {
        type: 'column',
        polar: false,
        inverted: false,
        width: null,
        plotBorderColor: '#cccccc'
      },
      yAxis: [{
        type: 'logarithmic',
        title: {
          style: '{ "color": "#666666" }',
          text: 'kWh'
        },
        labels: {
          format: '{value} kWh'
        },
        opposite: false,
        reversed: false,
        minorTickInterval: 'auto'
      }],
      title: {
        text: 'Weekly Consumption'
      },
      subtitle: {
        text: ''
      },
      exporting: {
        sourceWidth: 10
      },
      xAxis: [{
        opposite: true,
        reversed: false,
        title: {
          text: 'Week' + (noofdaysforChart + 1) + '(date this to date that)'
        },
        type: 'category',
        labels: {
          format: '{value}'
        }
      }],
      series: [{
        data: [
          ['Sunday', chartkwh[0]],
          ['Monday', chartkwh[1]],
          ['Tueday', chartkwh[2]],
          ['Wednesday', chartkwh[3]],
          ['Thursday', chartkwh[4]],
          ['Friday', chartkwh[5]],
          ['Saturday', chartkwh[6]],
        ],
        name: 'Kwh',
        turboThreshold: 0,
        _colorIndex: 0,
        _symbolIndex: 0,
        dashStyle: 'Solid',
        colorByPoint: false,
        marker: {
          enabled: true,
          symbol: 'diamond'
        },
        color: '#039be5',
        negativeColor: null
      }
      ],
      plotOptions: {
        series: {
          animation: true,
          dataLabels: {
            enabled: true,
            style: {
              color: 'contrast',
              fontSize: '11px',
              fontWeight: 'bold',
              textOutline: '1px 1px contrast'
            }
          }
        }
      },
      credits: {
        enabled: false
      },
      tooltip: {
        shared: false,
        enabled: false
      },
      colors: ['#7cb5ec', '#eceff1', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1'],
      legend: {
        enabled: true
      }
    }
    //end of sample
  }

  getPreviousDayData() {
    this.noofdaysforChart--;
    if (this.noofdaysforChart >= 0 && this.noofdaysforChart < this.meterdetails.endOfDayKwh.length)
      this.weeklyConsumptionChart(this.noofdaysforChart);
  }
  getNextDayData() {
    this.noofdaysforChart++;
    if (this.noofdaysforChart < this.meterdetails.endOfDayKwh.length && this.noofdaysforChart >= 0)
      this.weeklyConsumptionChart(this.noofdaysforChart);
  }
  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    this.loading.present();
  }
}

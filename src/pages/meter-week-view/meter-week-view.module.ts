import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeterWeekView } from './meter-week-view';

@NgModule({
  declarations: [
    MeterWeekView,
  ],
  imports: [
    IonicPageModule.forChild(MeterWeekView),
  ],
  exports: [
    MeterWeekView
  ]
})
export class MeterWeekViewModule {}

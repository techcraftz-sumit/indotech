import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'login-page',
    templateUrl: 'login-page.html'
})
export class LoginPage {
    email: string;
    password: string;
    loading: any;
    errormsg: any;

    constructor(public navCtrl: NavController, public authService: Auth, public loadingCtrl: LoadingController, public storage: Storage) {
    }

    /**
     * Function call after login button is clicked
     */
    login() {
        this.showLoader();
        //set credentials from submitted values to pass for authentication
        let credentials = {
            email: this.email,
            password: this.password
        };
        this.authService.login(credentials).then((result) => { //Function call to auth.ts for API
            this.loading.dismiss();
            this.GetUserDetails(); //Function call in same class  
        }, (err) => {
            this.loading.dismiss();
            let error = err.json();
            this.errormsg = error.message; // Display error messages
        });
    }

    /**
     * Check for user Authentication
     * Redirect user based on the response
     */
    GetUserDetails() {
        this.authService.checkAuthentication().then((res) => {
            console.log("Already authorized");
            this.navCtrl.setRoot(HomePage);
        }, (err) => {
            console.log("Not already authorized");
            this.navCtrl.setRoot(LoginPage);
        });
    }

    /**
    * Show content while authenticating
    */
    showLoader() {
        this.loading = this.loadingCtrl.create({
            content: 'Authenticating...'
        });
        this.loading.present();
    }

}
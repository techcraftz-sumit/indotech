import { Auth } from '../../providers/auth';
import { NavController, LoadingController, App } from 'ionic-angular';
import { LoginPage } from '../login-page/login-page';
import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'popover.html'
})

export class PopoverContentPage {
  username: any;

  constructor(public viewCtrl: ViewController, public storage: Storage, public authService: Auth, public navCtrl: NavController, protected app: App) {
    this.storage.get('user_details').then((val) => {
      this.username = val.name; //Set username to show in side menu
    });
  }

  /**
   * Function call on logout button click
   */
  logout() {
    this.authService.logout();
    this.viewCtrl.dismiss();
    this.app.getRootNav().setRoot(LoginPage);// Redirect to Login Page 
  }

  /**
   * Close Side Menu 
   */
  close() {
    this.viewCtrl.dismiss();
  }
}
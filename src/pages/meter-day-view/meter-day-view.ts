import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';

/**
 * Generated class for the MeterDayView page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-meter-day-view',
  templateUrl: 'meter-day-view.html',
})
export class MeterDayView {
  meterid: any;
  meterloaddetails: any;
  loadSurvey: any;
  loading: any;
  chartOptionsday: any;
  chartOptionsday1: any;
  band: any;
  noofdaysforChart = 0;
  loadSurveyDaysBack = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: Auth, public loadingCtrl: LoadingController) {
    this.meterid = this.navParams.data;
  }

  /**
   * Get meter Load Details based on meter id
   */
  ngOnInit() {
    this.showLoader();
    this.authService.meterloaddetails(this.meterid).then((res) => { //Function call to auth.ts for API
      this.meterloaddetails = res;
      this.dailyConsumptionChart(1);
      this.loading.dismiss();
    }, (err) => {
      console.log(err);
      this.loading.dismiss();
    });
    this.makeLoadSurveyChart(0);
  }

  /**
   * Daily meter Consumption Chart
   * @param noofdaysforChart 
   */
  dailyConsumptionChart(noofdaysforChart) {
    this.band = [0, 0, 0, 0];
    var sumOfAll = 0;
    var sumOfAll = 0;
    var totalNoZero = 0;
    for (var i = 0; i < this.meterloaddetails.kwh.length; i++) {
      if (this.meterloaddetails.kwh[i] != 0) {
        sumOfAll += this.meterloaddetails.kwh[i];
        totalNoZero++;
      }
    }
    this.band[0] = (sumOfAll / totalNoZero) * .4;
    this.band[1] = (sumOfAll / totalNoZero);
    this.band[2] = (sumOfAll / totalNoZero) * 1.6;
    this.band[3] = (sumOfAll / totalNoZero) * 2;

    this.chartOptionsday = {
      chart: {
        type: 'spline'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: 'kwh and kvah'
      },
      xAxis: {
        type: 'datetime',
        labels: {
          overflow: 'justify'
        }
      },
      yAxis: {
        title: {
          text: 'Load'
        },
        minorGridLineWidth: 0,
        gridLineWidth: 0,
        alternateGridColor: null,
        plotBands: [{
          from: 0,
          to: this.band[0],
          color: 'rgba(68, 170, 213, 0.1)',
          label: {
            text: 'Low Usage',
            style: {
              color: '#606060'
            }
          }
        }, {
          from: this.band[0],
          to: this.band[1],
          color: 'rgba(0, 0, 0, 0)',
          label: {
            text: 'Below Average Usage',
            style: {
              color: '#606060'
            }
          }
        }, {
          from: this.band[1],
          to: this.band[2],
          color: 'rgba(68, 170, 213, 0.1)',
          label: {
            text: 'Above Average Usage',
            style: {
              color: '#606060'
            }
          }
        }, {
          from: this.band[2],
          to: this.band[3],
          color: 'rgba(0, 0, 0, 0)',
          label: {
            text: 'Over Load',
            style: {
              color: '#606060'
            }
          }
        }]
      },
      tooltip: {
        valueSuffix: ' kw'
      },
      plotOptions: {
        spline: {
          lineWidth: 4,
          states: {
            hover: {
              lineWidth: 5
            }
          },
          marker: {
            enabled: false
          },
          pointInterval: 900000, // 15 minutes
          pointStart: Date.UTC(2017, 5, 4, 0, 0, 0)
        }
      },
      series: [{
        name: 'KW',
        data: []
      }],
      navigation: {
        menuItemStyle: {
          fontSize: '10px'
        }
      }
    }
    this.chartOptionsday.title.text = "Hourly consumption average for last " + (parseInt(noofdaysforChart)) + " day(s)"
    for (var i = 0; i < 96; i++) {
      this.chartOptionsday.series[0].data.push([this.meterloaddetails.kwh[i]]);
    }
  }


  makeLoadSurveyChart(daysBack) {
    this.authService.meterserialloaddetails(this.meterid, daysBack).then((res) => {
      this.loadSurvey = res;
      console.log(this.loadSurvey);
      this.band = [0, 0, 0, 0];
      var sumOfAll = 0;
      var totalNoZero = 0;
      for (var i = 0; i < this.loadSurvey.kwh.length; i++) {
        if (this.loadSurvey.kwh[i] != 0) {
          sumOfAll += this.loadSurvey.kwh[i];
          totalNoZero++;
        }
      }
      this.band[0] = (sumOfAll / totalNoZero) * .4;
      this.band[1] = (sumOfAll / totalNoZero);
      this.band[2] = (sumOfAll / totalNoZero) * 1.6;
      this.band[3] = (sumOfAll / totalNoZero) * 2;
      //sample json for highchart purpose to be built later
      var currentDate = this.loadSurvey.latestDate.substring(0, this.loadSurvey.latestDate.indexOf('T'))
      var first = currentDate.indexOf("-");
      var second = currentDate.indexOf("-", first + 1);
      var currentYear = this.loadSurvey.latestDate.substring(0, first);
      var currentMonth = this.loadSurvey.latestDate.substring(first + 1, second);
      var currentDateOnly = this.loadSurvey.latestDate.substring(second + 1, second + 3);
      this.chartOptionsday1 = {
        chart: {
          type: 'spline'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: 'kwh and kvah'
        },
        xAxis: {
          type: 'datetime',
          labels: {
            overflow: 'justify'
          }
        },
        yAxis: {
          title: {
            text: 'Load'
          },
          minorGridLineWidth: 0,
          gridLineWidth: 0,
          alternateGridColor: null,
          plotBands: [{
            from: 0,
            to: this.band[0],
            color: 'rgba(68, 170, 213, 0.1)',
            label: {
              text: 'Low Usage',
              style: {
                color: '#606060'
              }
            }
          }, {
            from: this.band[0],
            to: this.band[1],
            color: 'rgba(0, 0, 0, 0)',
            label: {
              text: 'Below Average Usage',
              style: {
                color: '#606060'
              }
            }
          }, {
            from: this.band[1],
            to: this.band[2],
            color: 'rgba(68, 170, 213, 0.1)',
            label: {
              text: 'Above Average Usage',
              style: {
                color: '#606060'
              }
            }
          }, {
            from: this.band[2],
            to: this.band[3],
            color: 'rgba(0, 0, 0, 0)',
            label: {
              text: 'Over Load',
              style: {
                color: '#606060'
              }
            }
          }]
        },
        tooltip: {
          valueSuffix: ' kw'
        },
        plotOptions: {
          spline: {
            lineWidth: 4,
            states: {
              hover: {
                lineWidth: 5
              }
            },
            marker: {
              enabled: false
            },
            pointInterval: 900000, // 15 minutes
            pointStart: Date.UTC(currentYear, currentMonth, currentDateOnly, 0, 0, 0)
          }
        },
        series: [{
          name: 'KW',
          data: []
        }],
        navigation: {
          menuItemStyle: {
            fontSize: '10px'
          }
        }
      }
      this.chartOptionsday1.title.text = "Hourly consumption for " + currentDate;
      for (var i = 0; i < 96; i++) {
        this.chartOptionsday1.series[0].data.push([this.loadSurvey.kwh[i]]);
      }

      //var chart = highcharts.chart('container2', highchartObject);
      // chart.redraw();
      // chart.reflow();
    }, (err) => {
      console.log(err);
    });
  }

  getPreviousDayData() {
    this.loadSurveyDaysBack++;
    this.makeLoadSurveyChart(this.loadSurveyDaysBack);
  }

  getNextDayData() {
    this.loadSurveyDaysBack--;
    this.makeLoadSurveyChart(this.loadSurveyDaysBack);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();
  }
}

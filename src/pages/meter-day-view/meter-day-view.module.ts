import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeterDayView } from './meter-day-view';

@NgModule({
  declarations: [
    MeterDayView,
  ],
  imports: [
    IonicPageModule.forChild(MeterDayView),
  ],
  exports: [
    MeterDayView
  ]
})
export class MeterDayViewModule {}

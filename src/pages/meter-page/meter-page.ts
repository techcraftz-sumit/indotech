import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login-page/login-page';
import { HomePage } from '../home/home';
import { MeterOverview } from '../meter-overview/meter-overview';
import { MeterDayView } from '../meter-day-view/meter-day-view';
import { MeterWeekView } from '../meter-week-view/meter-week-view';
import { PopoverController } from 'ionic-angular';
import { PopoverContentPage } from '../popover/popover';

@IonicPage()
@Component({
  selector: 'page-meter-page',
  templateUrl: 'meter-page.html',
})
export class MeterPage {
  public meterid: number;
  meteroverview = MeterOverview;
  meterdayview = MeterDayView;
  meterweekview = MeterWeekView;

  constructor(private navController: NavController, private navParams: NavParams, public authService: Auth, public popoverCtrl: PopoverController) {
    this.meterid = navParams.get('meterid'); // set meter id with value passed from home page
  }

  /**
   * Pop Over for Nav Controller
   * @param myEvent 
   */
  openPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverContentPage);
    popover.present({
      ev: myEvent
    });
  }
}

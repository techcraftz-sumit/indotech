import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeterOverview } from './meter-overview';

@NgModule({
  declarations: [
    MeterOverview,
  ],
  imports: [
    IonicPageModule.forChild(MeterOverview),
  ],
  exports: [
    MeterOverview
  ]
})
export class MeterOverviewModule {}

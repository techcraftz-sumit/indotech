import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';

/**
 * Generated class for the MeterOverview page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-meter-overview',
  templateUrl: 'meter-overview.html',
})
export class MeterOverview {
  meterid: any;
  meterserialno: any;
  metercumkwh: any;
  loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: Auth, public loadingCtrl: LoadingController) {
    this.meterid = this.navParams.data;
  }

  /**
   * Get meter basic details based on meter id
   */
  ngOnInit() {
    this.showLoader();
    this.authService.meterdetails(this.meterid).then((res) => { //Function call to auth.ts for API
      this.meterserialno = res[0].serialno;
      this.metercumkwh = res[0].cumKwh;
    }, (err) => {
      console.log(err);
    });
    this.loading.dismiss();

  }

  /**
  * Show Loading content while page load
   */
  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();
  }
}
